provider "google" {
  credentials = file("service-account.json")
  project = var.PROJECT
  region = var.REGION
  zone = "${var.REGION}-b"
}

terraform {
  backend "gcs" {
    bucket = "vcard-eu-gs-tfstate-01"
    prefix = "terraform/state/website"
  }
}


resource "google_cloud_run_service" "website" {
  name = "vcard-website"
  location = var.REGION
  project = var.PROJECT
  template {
    metadata {
      annotations = {
        "run.googleapis.com/execution-environment" = "gen1"
        "autoscaling.knative.dev/maxScale" = var.MAX_SCALE
        "current-desired-image" = var.DESIRED_IMAGE
      }
    }
    spec {
      containers {
        image = var.IMAGE
        env {
          name = "PROJECT_ID"
          value = var.PROJECT
        }
        ports {
          name = "http1"
          container_port = 8000
        }
        resources {
          limits = {
            "cpu" = "1000m"
            "memory" = var.MEMORY
          }
        }
      }
      service_account_name = var.PROJECT_SERVICE_ACCOUNT
    }
  }
  traffic {
    percent = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.website.location
  project     = google_cloud_run_service.website.project
  service     = google_cloud_run_service.website.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
