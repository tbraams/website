# Virtual business card
This Git is a CI/CD pipeline for my vCard at [braams.dev](https://braams.dev).  
The stack is very simple:
- Frontend: Bootstrap HTML, JS & CSS 
- Backend: Flask with a gunicorn WSGI (Python)
- Hosting: Docker container on GCP Cloud Run

The deployment is automated via GitLab CI and infrastructure on GCP is handled by Terraform.